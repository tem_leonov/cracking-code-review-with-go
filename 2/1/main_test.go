package ex_2_1

import (
	"crackingCodeReviewWithGo/common/linkedList"
	"testing"
)

func TestRemoveDuplicates(t *testing.T) {
	t.Run("Should return original array where it has no duplicates", func(t *testing.T) {
		list := linkedList.Create([]interface{}{2, 4, 3, 1, 5})
		expected := linkedList.Create([]interface{}{2, 4, 3, 1, 5})

		RemoveDuplicates(list)

		if !list.IsEqualTo(expected) {
			t.Errorf("Expected:\n%v\nBut actual:%v", expected, list)
		}
	})

	t.Run("Should return array without duplicates where array has duplicates", func(t *testing.T) {
		testCases := []struct {
			List     *linkedList.List
			Expected *linkedList.List
		}{
			{List: linkedList.Create([]interface{}{2, 2, 4, 3, 1, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 4, 3, 1, 5, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 4, 3, 4, 1, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 2, 4, 4, 3, 3, 1, 1, 5, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 4, 3, 1, 5, 4, 1, 3, 5, 2}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
		}

		for _, testCase := range testCases {
			list := testCase.List
			expected := testCase.Expected

			RemoveDuplicates(list)

			if !list.IsEqualTo(expected) {
				t.Errorf("Expected:\n%v\nBut actual:%v", expected, list)
			}
		}
	})
}

func TestRemoveDuplicatesWithoutMemoryBuffer(t *testing.T) {
	t.Run("Should return original array where it has no duplicates", func(t *testing.T) {
		list := linkedList.Create([]interface{}{2, 4, 3, 1, 5})
		expected := linkedList.Create([]interface{}{2, 4, 3, 1, 5})

		RemoveDuplicatesWithoutMemoryBuffer(list)

		if !list.IsEqualTo(expected) {
			t.Errorf("Expected:\n%v\nBut actual:%v", expected, list)
		}
	})

	t.Run("Should return array without duplicates where array has duplicates", func(t *testing.T) {
		testCases := []struct {
			List     *linkedList.List
			Expected *linkedList.List
		}{
			{List: linkedList.Create([]interface{}{2, 2, 4, 3, 1, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 4, 3, 1, 5, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 4, 3, 4, 1, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 2, 4, 4, 3, 3, 1, 1, 5, 5}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
			{List: linkedList.Create([]interface{}{2, 4, 3, 1, 5, 4, 1, 3, 5, 2}), Expected: linkedList.Create([]interface{}{2, 4, 3, 1, 5})},
		}

		for _, testCase := range testCases {
			list := testCase.List
			expected := testCase.Expected

			RemoveDuplicatesWithoutMemoryBuffer(list)

			if !list.IsEqualTo(expected) {
				t.Errorf("Expected:\n%v\nBut actual:%v", expected, list)
			}
		}
	})
}
