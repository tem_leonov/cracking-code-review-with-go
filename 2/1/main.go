package ex_2_1

import "crackingCodeReviewWithGo/common/linkedList"

func RemoveDuplicates(list *linkedList.List) {
	var existingValues = make(map[int]struct{})
	var prev *linkedList.Node
	var current = list.Start

	for current != nil {
		var _, ok = existingValues[current.Data.(int)]

		if !ok {
			existingValues[current.Data.(int)] = struct{}{}
			prev = current
		} else {
			removeNext(prev)
		}

		current = current.Next
	}
}

func RemoveDuplicatesWithoutMemoryBuffer(list *linkedList.List) {
	var current = list.Start

	for current != nil {
		prevCursor := current
		cursor := current.Next

		for cursor != nil {
			if current.Data == cursor.Data {
				removeNext(prevCursor)
			} else {
				prevCursor = cursor
			}

			cursor = cursor.Next
		}

		current = current.Next
	}
}

func removeNext(node *linkedList.Node) {
	if node.Next != nil {
		node.Next = node.Next.Next
	}
}
