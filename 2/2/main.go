package ex_2_2

import (
	"crackingCodeReviewWithGo/common/emptyError"
	"crackingCodeReviewWithGo/common/linkedList"
)

func FindElement(list *linkedList.List, indexFromEnd int) (int, error) {
	if list == nil {
		return 0, &emptyError.Error{}
	}

	current := list.Start

	for i := 0; i < indexFromEnd; i++ {
		if current == nil {
			return 0, &emptyError.Error{}
		}

		current = current.Next
	}

	cursor := list.Start

	for current != nil {
		current = current.Next
		cursor = cursor.Next
	}

	if cursor == nil {
		return 0, &emptyError.Error{}
	}

	return cursor.Data.(int), nil
}
