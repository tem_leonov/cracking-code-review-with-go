package ex_2_2

import (
	"crackingCodeReviewWithGo/common/linkedList"
	"testing"
)

func TestFindElement(t *testing.T) {
	t.Run("Should return err when k is invalid", func(t *testing.T) {
		testCases := []struct {
			List        *linkedList.List
			K           int
			Expected    int
			ExpectError bool
		}{
			{List: linkedList.Create([]interface{}{1, 2, 3, 4, 5}), K: 0, ExpectError: true},
			{List: linkedList.Create([]interface{}{1, 2, 3, 4, 5}), K: -1, ExpectError: true},
			{List: linkedList.Create([]interface{}{1, 2, 3, 4, 5}), K: 6, ExpectError: true},
			{List: linkedList.Create([]interface{}{}), K: 1, ExpectError: true},
		}

		for _, testCase := range testCases {
			test(t, &testCase)
		}
	})

	t.Run("Should return k from end element when k is valid", func(t *testing.T) {
		testCases := []struct {
			List        *linkedList.List
			K           int
			Expected    int
			ExpectError bool
		}{
			{List: linkedList.Create([]interface{}{1, 2, 3, 4, 5}), K: 1, ExpectError: false, Expected: 5},
			{List: linkedList.Create([]interface{}{1, 2, 3, 4, 5}), K: 3, ExpectError: false, Expected: 3},
			{List: linkedList.Create([]interface{}{1, 2, 3, 4, 5}), K: 5, ExpectError: false, Expected: 1},
		}

		for _, testCase := range testCases {
			test(t, &testCase)
		}
	})
}

func test(
	t *testing.T,
	testCase *struct {
		List        *linkedList.List
		K           int
		Expected    int
		ExpectError bool
	}) {
	element, err := FindElement(testCase.List, testCase.K)

	if testCase.ExpectError && err == nil {
		t.Errorf("Expected error, but returned %v", element)
	}

	if testCase.Expected != element {
		t.Errorf("Expected %v, but was %v", testCase.Expected, element)
	}
}
