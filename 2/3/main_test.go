package ex_2_3

import (
	"crackingCodeReviewWithGo/common/linkedList"
	"testing"
)

func TestRemoveFromTheMiddle(t *testing.T) {
	t.Run("Should remove element when found", func(t *testing.T) {
		testCases := []struct {
			List          *linkedList.List
			IndexToDelete int
			Expected      *linkedList.List
		}{
			{List: linkedList.Create([]interface{}{1, 2, 3, 4}), IndexToDelete: 2, Expected: linkedList.Create([]interface{}{1, 3, 4})},
			{List: linkedList.Create([]interface{}{1, 2, 3, 4}), IndexToDelete: 3, Expected: linkedList.Create([]interface{}{1, 2, 4})},
		}

		for _, testCase := range testCases {
			node := testCase.List.Start

			for i := 0; i < testCase.IndexToDelete-1; i++ {
				node = node.Next
			}

			RemoveFromTheMiddle(testCase.List, node)

			if !testCase.List.IsEqualTo(testCase.Expected) {
				t.Errorf("Expected:\n%v\nBut actual:%v", testCase.Expected, testCase.List)
			}
		}
	})

	t.Run("Should do nothing when element not found", func(t *testing.T) {
		testCases := []struct {
			List     *linkedList.List
			Node     *linkedList.Node
			Expected *linkedList.List
		}{
			{List: linkedList.Create([]interface{}{1, 2, 3, 4}), Node: &linkedList.Node{Data: 5}, Expected: linkedList.Create([]interface{}{1, 2, 3, 4})},
		}

		for _, testCase := range testCases {
			RemoveFromTheMiddle(testCase.List, testCase.Node)

			if !testCase.List.IsEqualTo(testCase.Expected) {
				t.Errorf("Expected:\n%v\nBut actual:%v", testCase.Expected, testCase.List)
			}
		}
	})
}
