package ex_2_3

import "crackingCodeReviewWithGo/common/linkedList"

func RemoveFromTheMiddle(list *linkedList.List, node *linkedList.Node) {
	if list != nil && list.Start != nil && list.Start.Next != nil {
		prev := list.Start
		current := prev.Next

		for current != nil {
			if current == node {
				prev.Next = current.Next
				return
			}

			prev = current
			current = current.Next
		}
	}
}
