package linkedList

import "fmt"

type Node struct {
	Data interface{}
	Next *Node
}

type List struct {
	Start *Node
}

func Create(datas []interface{}) (list *List) {
	var current *Node

	for _, data := range datas {
		node := &Node{Data: data}

		if current == nil {
			list = &List{Start: node}
			current = list.Start
		} else {
			current.Next = node
			current = current.Next
		}
	}

	return
}

func (list *List) String() string {
	result := "{ "
	var current = list.Start
	printPostfix := false

	for current != nil {
		if printPostfix {
			result += ", "
		} else {
			printPostfix = true
		}

		result += fmt.Sprintf("%v", current.Data)

		current = current.Next
	}

	return result + " }"
}

func (list *List) IsEqualTo(other *List) bool {
	var currentA = list.Start
	var currentB = other.Start

	for currentA != nil {
		if currentB == nil || currentA.Data != currentB.Data {
			return false
		}

		currentA = currentA.Next
		currentB = currentB.Next
	}

	return true
}
