package main

// IsStringRearrangement returns whether b is rearrangement of a
func IsStringRearrangement(a string, b string) bool {
	if len(a) != len(b) {
		return false
	}

	symbolRep := make(map[rune]uint16)

	for _, r := range []rune(a) {
		if _, ok := symbolRep[r]; !ok {
			symbolRep[r] = 1
		} else {
			symbolRep[r]++
		}
	}

	interrupted := false

	for _, r := range []rune(b) {
		if _, ok := symbolRep[r]; !ok {
			interrupted = true
			break
		} else {
			symbolRep[r]--

			if symbolRep[r] == 0 {
				delete(symbolRep, r)
			}
		}
	}

	if interrupted {
		return false
	}

	return len(symbolRep) == 0
}
