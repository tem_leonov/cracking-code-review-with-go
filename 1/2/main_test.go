package main

import (
	"testing"
)

func TestIsStringRearrangment(t *testing.T) {
	testMatrix := []*struct {
		a        string
		b        string
		expected bool
	}{
		{"работа", "работа", true},
		{"работа", "табора", true},
		{"работа", "работб", false},
		{"работа", "работарь", false},
		{"работа", "раб", false},
		{"работа", "авытаыврыашв", false},
	}

	for _, item := range testMatrix {
		actual := IsStringRearrangement(item.a, item.b)

		if item.expected != actual {
			t.Errorf("Expected: %v, but actual: %v", item.expected, actual)
		}
	}
}
