package main

import (
	"testing"
)

func TestIsPalindromePermutation(t *testing.T) {
	testMatrix := []*struct {
		str      string
		expected bool
	}{
		{"Tact Coa", true},
		{"atco cta", true},
		{"cabcab", true},
		{"Is this a palindrome", false},
		{"", false},
	}

	for _, item := range testMatrix {
		actual := IsPalindromePermutation(item.str)

		if actual != item.expected {
			t.Errorf("For argue %v expected: %v, but actual: %v", item.str, item.expected, actual)
		}
	}
}
