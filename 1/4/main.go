package main

import (
	"strings"
)

func IsPalindromePermutation(str string) bool {
	if len(str) == 0 {
		return false
	}

	symbolsCount := make(map[rune]uint16)
	len := 0

	for _, r := range []rune(strings.ToLower(str)) {
		if r != ' ' {
			symbolsCount[r]++
			len++
		}
	}

	unpairedLeft := 0

	if len%2 == 1 {
		unpairedLeft = 1
	}

	result := true

	for _, v := range symbolsCount {
		if v%2 != 0 {
			unpairedLeft--

			if unpairedLeft < 0 {
				result = false
				break
			}
		}
	}

	return result
}
