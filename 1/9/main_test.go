package main

import (
	"testing"
)

func TestIsCyclicShift(t *testing.T) {
	testCases := []struct {
		A        string
		B        string
		Expected bool
	}{
		{"waterbottle", "erbottlewat", true},
		{"waterbottle", "waterbottle", true},
		{"waterbottle", "sodabottle", false},
		{"waterbottle", "erbottlewaterbottlewat", false},
		{"waterbottle", "erbottlewaterbottlewaterbottlewat", false},
	}

	for _, testCase := range testCases {
		actual := IsCyclicShift(testCase.A, testCase.B)

		if testCase.Expected != actual {
			t.Errorf("Expected is %v, but actual is %v", testCase.Expected, actual)
		}
	}
}
