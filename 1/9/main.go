package main

import (
	"strings"
)

func IsCyclicShift(a, b string) bool {
	if len(a) != len(b) {
		return false
	}

	testString := a + a

	if !strings.Contains(testString, b) {
		return false
	}

	testString = strings.Replace(testString, b, "", 1)

	return a == testString
}
