package main

import (
	"strconv"
	"strings"
)

func writeCompressed(b *strings.Builder, prevChar rune, repCount int) {
	if repCount > 0 {
		b.WriteRune(prevChar)
		b.WriteString(strconv.Itoa(repCount))
	}
}

// Compress string if it is efficient
func Compress(str string) string {
	b := strings.Builder{}

	var prevChar rune
	i := 0

	for _, r := range []rune(str) {
		if prevChar != r {
			writeCompressed(&b, prevChar, i)

			i = 1
			prevChar = r
		} else {
			i++
		}
	}

	writeCompressed(&b, prevChar, i)

	result := b.String()

	if len(result) > len(str) {
		result = str
	}

	return result
}
