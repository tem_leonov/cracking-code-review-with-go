package main

import (
	"testing"
)

func TestCompress(t *testing.T) {
	testMatrix := []struct {
		str      string
		expected string
	}{
		{"aabcccccaaa", "a2b1c5a3"},
		{"abbbc", "abbbc"},
		{"a", "a"},
	}

	for _, testCase := range testMatrix {
		actual := Compress(testCase.str)

		if testCase.expected != actual {
			t.Errorf("Expected %v, but actual is %v", testCase.expected, actual)
		}
	}
}
