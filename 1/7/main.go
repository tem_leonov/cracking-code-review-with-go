package main

type Pixel struct {
	R byte
	G byte
	B byte
	A byte
}

func Turn(img [][]Pixel) {
	layersCount := len(img) / 2 + len(img) % 2

	for i := 0; i < layersCount; i++ {
		TurnLayer(img, i)
	}
}

func TurnLayer(img [][]Pixel, layerNum int) {
	y := layerNum
	jumpDistance := len(img) - 2*layerNum - 1

	if jumpDistance > 0 {
		for x := layerNum; x < layerNum + jumpDistance; x++ {
			TurnLayerElem(img, y, x, jumpDistance)
		}
	}
}

func TurnLayerElem(img [][]Pixel, y int, x int, jumpDistance int) {
	drY, drX := jumpDistance - y, jumpDistance - x
	trY, trX := x, drY
	dlY, dlX := drX, y
	tmp := img[y][x]
	img[y][x] = img[dlY][dlX]
	img[dlY][dlX] = img[drY][drX]
	img[drY][drX] = img[trY][trX]
	img[trY][trX] = tmp
}