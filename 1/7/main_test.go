package main

import (
	"testing"
)

func TestTurn(t *testing.T) {
	testCases := []struct {
		Pixels   [][]Pixel
		Expected [][]Pixel
	}{
		{
			[][]Pixel{
				{{2, 6, 8, 1}},
			},
			[][]Pixel{
				{{2, 6, 8, 1}},
			},
		},

		{
			[][]Pixel{
				{{2, 6, 8, 1}, {4, 9, 5, 2}},
				{{2, 5, 4, 9}, {5, 8, 2, 1}},
			},
			[][]Pixel{
				{{2, 5, 4, 9}, {2, 6, 8, 1}},
				{{5, 8, 2, 1}, {4, 9, 5, 2}},
			},
		},

		{
			[][]Pixel{
				{{2, 6, 8, 1}, {4, 9, 5, 2}, {5, 3, 2, 5}},
				{{2, 5, 4, 9}, {5, 8, 2, 1}, {9, 3, 8, 9}},
				{{4, 6, 2, 3}, {5, 7, 8, 3}, {9, 2, 6, 3}},
			},
			[][]Pixel{
				{{4, 6, 2, 3}, {2, 5, 4, 9}, {2, 6, 8, 1}},
				{{5, 7, 8, 3}, {5, 8, 2, 1}, {4, 9, 5, 2}},
				{{9, 2, 6, 3}, {9, 3, 8, 9}, {5, 3, 2, 5}},
			},
		},
	}

	for _, testCase := range testCases {
		actual := testCase.Pixels
		Turn(actual)

		if !ImagesAreEqual(testCase.Expected, actual) {
			t.Errorf("Expected:\n%v\n\nActual:\n%v", testCase.Expected, actual)
		}
	}
}

func ImagesAreEqual(a [][]Pixel, b [][]Pixel) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		if len(a[i]) != len(b[i]) {
			return false
		}

		for j := 0; j < len(a[i]); j++ {
			pixelA := a[i][j]
			pixelB := b[i][j]

			if pixelA.R != pixelB.R || pixelA.G != pixelB.G || pixelA.B != pixelB.B || pixelA.A != pixelB.A {
				return false
			}
		}
	}

	return true
}
