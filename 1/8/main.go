package main

// Zerofy all matrix rows and columns that contains at least one 0 element
func Zerofy(m [][]int) {
	zRows := make(map[int]bool)
	zCols := make(map[int]bool)

	for row := 0; row < len(m); row++ {
		_, contains := zRows[row]

		if !contains {
			for col := 0; col < len(m[row]); col++ {
				_, contains := zCols[col]

				if !contains {
					if m[row][col] == 0 {
						zerofyRow(m, row)
						zRows[row] = true
						zerofyCol(m, col)
						zCols[col] = true
						break
					}
				}
			}
		}
	}
}

func zerofyRow(m [][]int, row int) {
	for col := 0; col < len(m[row]); col++ {
		m[row][col] = 0
	}
}

func zerofyCol(m [][]int, col int) {
	for row := 0; row < len(m); row++ {
		m[row][col] = 0
	}
}
