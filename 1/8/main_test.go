package main

import (
	"testing"
)

func TestZerofy(t *testing.T) {
	testCases := []struct {
		M        [][]int
		Expected [][]int
	}{
		{
			[][]int{
				{4, 5, 2, 1},
				{3, 5, 7, 8},
				{9, 2, 1, 8},
				{5, 2, 8, 9},
				{-2, 5, 7, 2},
			},
			[][]int{
				{4, 5, 2, 1},
				{3, 5, 7, 8},
				{9, 2, 1, 8},
				{5, 2, 8, 9},
				{-2, 5, 7, 2},
			},
		},
		{
			[][]int{
				{4, 5, 9, 0},
				{2, 4, 5, 5},
				{3, 2, 1, 1},
			},
			[][]int{
				{0, 0, 0, 0},
				{2, 4, 5, 0},
				{3, 2, 1, 0},
			},
		},
		{
			[][]int{
				{9, 2, 3, 0},
				{6, 0, 2, 1},
				{0, 3, 2, 1},
				{8, 4, 5, 2},
			},
			[][]int{
				{0, 0, 0, 0},
				{0, 0, 0, 0},
				{0, 0, 0, 0},
				{0, 0, 5, 0},
			},
		},
	}

	for _, testCase := range testCases {
		actual := testCase.M
		Zerofy(actual)

		if !SlicesAreEqual(testCase.Expected, actual) {
			t.Errorf("Expected:\n%v\nBut actual:%v", testCase.Expected, actual)
		}
	}
}

func SlicesAreEqual(a [][]int, b [][]int) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		if len(a[i]) != len(b[i]) {
			return false
		}

		for j := 0; j < len(a[i]); j++ {
			itemA := a[i][j]
			itemB := b[i][j]

			if itemA != itemB {
				return false
			}
		}
	}

	return true
}
