package main

import (
	"testing"
)

func TestIsStringHasDuplicateCharacters(t *testing.T) {
	t.Run("Should return false when specified string has not duplicated characters", func(t *testing.T) {
		str := "абвгдежзик"
		expected := false

		actual := IsStringHasDuplicateCharacters(str)

		if !actual {
			t.Errorf("Expected: %v, but actual: %v", expected, actual)
		}
	})
	t.Run("Should return true when specified string has duplicated characters", func(t *testing.T) {
		strings := []string{"работа", "квинтэссенция"}
		expected := true

		for _, s := range strings {
			actual := IsStringHasDuplicateCharacters(s)

			if !actual {
				t.Errorf("Expected: %v, but actual: %v", expected, actual)
			}
		}
	})
}
