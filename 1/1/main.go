package main

// IsStringHasDuplicateCharacters returns true when specified string have duplicated characters
// and false if not
func IsStringHasDuplicateCharacters(str string) bool {
	s := []byte(str)
	res := false

	for i := 1; i < len(s); i++ {
		for j := i; j > 0; j-- {
			if s[j] < s[j-1] {
				swapCharacters(s, j, j-1)
			} else if s[j] == s[i] {
				res = true
				break
			}
		}

		if res {
			break
		}
	}

	return res
}

func swapCharacters(str []byte, i1 int, i2 int) {
	tmp := str[i1]
	str[i1] = str[i2]
	str[i2] = tmp
}
