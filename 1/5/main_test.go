package main

import (
	"testing"
)

func TestIsStringsInOneModificationDistance(t *testing.T) {
	testMatrix := []struct {
		a        string
		b        string
		expected bool
	}{
		{"pale", "ple", true},
		{"pale", "pales", true},
		{"pale", "bale", true},
		{"pale", "pale", true},
		{"pale", "bake", false},
		{"pale", "paless", false},
	}

	for _, item := range testMatrix {
		actual := IsStringsInOneModificationDistance(item.a, item.b)

		if actual != item.expected {
			t.Errorf(
				"For argues: \"%v\", \"%v\" expected: %v, but actual: %v",
				item.a,
				item.b,
				item.expected,
				actual,
			)
		}
	}
}
