package main

import (
	"math"
)

// IsStringsInOneModificationDistance returns true when string a
// is in distance of one modification from string b
func IsStringsInOneModificationDistance(a string, b string) bool {
	if a == b {
		return true
	}

	if math.Abs(float64(len(a)-len(b))) > 1 {
		return false
	}

	aMods := createStringMods(a)
	bMods := createStringMods(b)

	result := false

	for i := 0; i < len(aMods); i++ {
		for j := 0; j < len(bMods); j++ {
			if aMods[i] == bMods[j] {
				result = true
				break
			}
		}
	}

	return result
}

func createStringMods(str string) []string {
	mods := make([]string, len(str)+1)

	for i := 0; i < len(str); i++ {
		mods[i] = createStringMod(str, i)
	}

	mods[len(str)] = str

	return mods
}

func createStringMod(str string, exIndex int) string {
	strRunes := []rune(str)
	mod := make([]rune, len(str)-1)
	j := 0

	for i := 0; i < len(strRunes); i++ {
		if i != exIndex {
			mod[j] = strRunes[i]
			j++
		}
	}

	return string(mod)
}
