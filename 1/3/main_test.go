package main

import (
	"testing"
)

func TestEncodeSpaces(t *testing.T) {
	testMatrix := []*struct {
		str string
		len uint16
		expected string
	} {
		{"Mr John Smith    ", 13, "Mr%20John%20Smith"},
		{"MrJohnSmith", 11, "MrJohnSmith"},
		{"Mr  John Smith      ", 14, "Mr%20%20John%20Smith"},
	}

	for _, item := range testMatrix {
		actual := EncodeSpaces(item.str, item.len)

		if actual != item.expected {
			t.Errorf("Expected: %v, but actual: %v", item.expected, actual)
		}
	}
}