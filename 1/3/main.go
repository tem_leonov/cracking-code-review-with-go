package main

func EncodeSpaces(str string, length uint16) string {
	arr := []rune(str)
	spacesCount := countSpaces(arr[:length])
	prepareArray(arr, spacesCount)

	return string(arr)
}

func shiftRight(arr []rune, step int) {
	for i := len(arr) - 1; i-step >= 0; i-- {
		arr[i] = arr[i-step]
	}
}

func prepareArray(arr []rune, spacesCount uint32) {
	step := int(spacesCount * 2)
	state := 0

	for i := len(arr) - 1; step > 0; i-- {
		if state == 0 {
			if arr[i-step] == ' ' {
				state = 1
				arr[i] = '0'
			} else {
				arr[i] = arr[i-step]
			}
		} else if state == 1 {
			state = 2
			arr[i] = '2'
		} else if state == 2 {
			state = 0
			arr[i] = '%'
			step -= 2
		}
	}
}

func countSpaces(arr []rune) uint32 {
	var count uint32 = 0

	for _, r := range arr {
		if r == ' ' {
			count++
		}
	}

	return count
}
